// COMS20001 - Cellular Automaton Farm - Initial Code Skeleton
// (using the XMOS i2c accelerometer demo)

#include <platform.h>
#include <xs1.h>
#include <stdio.h>
#include <string.h>
#include "pgmIO.h"
#include "i2c.h"

//-----register addresses for accelerometer-----//
#define FXOS8700EQ_I2C_ADDR 0x1E
#define FXOS8700EQ_XYZ_DATA_CFG_REG 0x0E
#define FXOS8700EQ_CTRL_REG_1 0x2A
#define FXOS8700EQ_DR_STATUS 0x0
#define FXOS8700EQ_OUT_X_MSB 0x1
#define FXOS8700EQ_OUT_X_LSB 0x2
#define FXOS8700EQ_OUT_Y_MSB 0x3
#define FXOS8700EQ_OUT_Y_LSB 0x4
#define FXOS8700EQ_OUT_Z_MSB 0x5
#define FXOS8700EQ_OUT_Z_LSB 0x6

//-----Program parameters-----//
#define  IMHT 1440                //image height
#define  IMWD 960                  //image width
#define  chunkNum 8
#define chunkHeight IMHT/chunkNum + 2
#define  height IMHT/chunkNum
#define tenth IMHT/10
const int h = (int)height;
const int lines = (int)IMWD/8;

#define infname "test.pgm"
#define outfname "testout.pgm"
#define roundLimit 100

//-----custom structures for readability-----//
typedef unsigned char uchar;
//holds groups of lines to pass to workers
typedef struct{
  uchar pixels[chunkHeight][IMWD/8];
} Chunk;

//Holds top and bottom overlapping lines
typedef struct{
  uchar pixels[2][IMWD/8];
} Overlap;

//controls access to the image by workers
typedef interface pixelMemory{
  void storeByte(int x, int y,uchar byte);
  uchar getBit(int x, int y);
  int getPixelCount();
  Overlap getOverlap();
  void storeLine(int y, uchar line[IMWD/8]);
  void storeOverlap(Overlap overlap);
  Chunk getChunk(int c);
} pixelMemory;

on tile[0] : port p_scl = XS1_PORT_1E;         //interface ports to accelerometer
on tile[0] : port p_sda = XS1_PORT_1F;
on tile[0] : port buttons = XS1_PORT_4E; //port to access buttons
on tile[0] : port leds = XS1_PORT_4F; //port to access LEDs

//-----Worker's helper functions-----//
//gets a single pixel from a worker's chunk
int getBitFromChunk(int x, int y, uchar c[chunkHeight][IMWD/8]){
  int i = x%8;
  x = x/8;
  if((c[y][x]>>i)&1){
    return 1;
  }
  return 0;
}

//counts a pixel's neighbours and determines if dies or lives
int isAlive(int x, int y, uchar c[chunkHeight][IMWD/8]){
  int neighbours = 0;
  //Above
  neighbours+= getBitFromChunk(x,y+1, c);
  //Below
  neighbours+= getBitFromChunk(x,y-1, c);
  //Right
  neighbours+= getBitFromChunk((x+1)%IMWD,y, c);
  //Left
  neighbours+= getBitFromChunk((x+IMWD-1)%IMWD,y, c);
  //Top Right
  neighbours+= getBitFromChunk((x+1)%IMWD,y+1, c);
  //Bottom Left
  neighbours+= getBitFromChunk((x+IMWD-1)%IMWD,y-1, c);
  //Top Left
  neighbours+= getBitFromChunk((x+IMWD-1)%IMWD,y+1, c);
  //Bottom Right
  neighbours+= getBitFromChunk((x+1)%IMWD,y-1, c);
  //printf("got here :) \n");

  if(neighbours < 2 || neighbours > 3){
    return 0;
  }else if(neighbours == 3){
    return 1;
  }else{
    return getBitFromChunk(x,y,c);
  }
}
//-----Worker thread that processes new image -----//
//pulls a chunk with overlaps from main image and determines new pixels
void workerThread(int id, chanend toDist, client pixelMemory MM){
  int running = 1;
  if(id>=(chunkNum/2)){
    id = id-(chunkNum/2);
  }
  //printf("worker %d is running \n", id);
  //uchar pixels[8];
  uchar byte = 0;
  uchar line[IMWD/8];
  Chunk oldChunk;
  while(running){
    toDist :>  running;
    //grab chunk from image
    oldChunk = MM.getChunk(id);
    //printf("Worker %d got chunk\n", id);
    toDist :>  int i;
    //find new pixel values, 8 pixels at a time, then store them back in the image
    for(int y=1;y <= h;y++){
      for(int x=0; x < IMWD;x++){
        if(isAlive(x,y, oldChunk.pixels)){
          byte += 1<<(x%8);
        }
        if(x%8 == 7 && x!=0){
            line[x/8] = byte;
            byte = 0;
        }
      }
      MM.storeLine((id*height)+y-1, line);
    }
    toDist <: 1;
  }
  printf("worker thread %d closed\n", id);
  return;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
// Read Image from PGM file from path infname[] to channel c_out
//
/////////////////////////////////////////////////////////////////////////////////////////
void DataInStream( chanend c_out)
{
  int res;
  uchar line[ IMWD ];
  printf( "DataInStream: Start...\n" );
  //Open PGM file
  res = _openinpgm( infname, IMWD, IMHT );
  if( res ) {
    printf( "DataInStream: Error openening %s\n.", infname );
    return;
  }
  //Read image line-by-line and send byte by byte to channel c_out
  for( int y = 0; y < IMHT; y++ ) {
    _readinline( line, IMWD );
    for( int x = 0; x < IMWD; x++ ) {
      c_out <: line[x];
      //printf( "-%4.1d ", line[ x ] ); //show image values
    }
  }
  //Close PGM image file
  _closeinpgm();
  printf( "DataInStream:Done...\n" );
  return;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
//Reads the image in 2 halves. Synchronises workers with I/O.
//Exports new image when stopped
//
/////////////////////////////////////////////////////////////////////////////////////////
void distributor(chanend c_in, chanend c_out, chanend fromAcc, chanend fromButtons, chanend cWorker[chunkNum], client pixelMemory MM,client pixelMemory MM2,out port c_leds)
{
  printf("Chunk Height:%d, Chunk Count:%d, Chunk Width:%d",h,chunkNum,IMWD);
  printf( "ProcessImage:Start, size = %dx%d\n", IMHT, IMWD );
  printf( "Waiting for button press...\n" );
  int running = 1;
  int rounds = 0;
  int paused = 0;
  int scale = 100000;
  int addTime = 0;
  unsigned int start, current, lastCurrent = 0, lastPause, time = 0, maxTime = 4294967295/scale;
  uchar val;
  int value = 0;
  while(value != 14){
    fromButtons :> value;
  }

  //Turn on RGB LED with green state
  c_leds<: 4;
  printf( "Processing...\n" );

  int progress = 0;
  uchar byte = 0;
  //retrieves pixels from dataInStream, and store them 8 pixels at a time in the memory manager
  for( int y = 0; y < IMHT; y++ ) {
    for( int x = 0; x < IMWD; x++ ) {
      c_in :> val;
      if(val == 255){
        byte += 1<<(x%8);
      }
      if(x%8 == 7 && x!=0){
        if(y >= (IMHT/2)){
          MM2.storeByte(x, y-(IMHT/2), byte);
        }else{
          MM.storeByte(x, y, byte);
        }
        byte = 0;
      }
    }
    if(y > progress*tenth){
      printf("reading... %d0%% \n", progress);
      progress++;
    }
  }

  printf("finished reading in file \n");
  //change LED colour
  c_leds<: 0;

  printf("telling workers to pull chunks\n");
  timer t;
  t :> start; //Start timer
  start = start/scale;
  t :> lastPause;
  lastPause = lastPause/scale;]


  // repeated processing rounds

  while(running){j
    t :> current;
    current = current/scale;
    if (lastCurrent > current){
      //printf("Starting timer over\n");
      addTime += 1;
    }
    lastCurrent = current;
    if(rounds >= roundLimit&&roundLimit!=0){
      running = 0;
    }else{
      select{
        //Paused State
        case fromAcc :> paused:
        if(paused){
          c_leds<: 8; //RGB LED to red state while paused
          if(addTime != 0){
            time += ((maxTime*addTime)-lastPause) + current; //Change time depending on how many times the clock has restarted since last pause
            addTime = 0;
          }
          else{
            time += current - lastPause; //Get current time and compare it with start time
          }
          printf("\nRounds processed: %d\n",rounds);
          printf("Current number of live cells: %d\n",MM.getPixelCount());
          printf("Processing time elapsed: %d%s%d seconds\n",time/1000,".",time%1000);

        }
        else
        c_leds<: 0;
        lastPause = current;

        break;
        //Stopped state
        case fromButtons :> value:
        if(!paused){
          if(value == 13){
            running = 0;
          }
        }
        break;

        //Running state
        default:
        if(!paused){
          //Flash green LED on and off
          c_leds <: (rounds%2);
          if(chunkNum == 8){
          MM.storeOverlap(MM2.getOverlap());
          MM2.storeOverlap(MM.getOverlap());
          }
          //Tell workers to take chunks from memory manager
          for(int i=0;i < chunkNum; i++){
            cWorker[i] <: 1;
          }
          //tell workers to start working
          int complete = 0;
          for(int i=0;i < chunkNum; i++){
            cWorker[i] <: 1;
          }
          //check if workers have finished before new processing round
          for(int i=0;i < chunkNum; i++){
            cWorker[i] :> complete;
          }

          rounds++;
        }
        break;
      }
    }
  }
  t :> current;
  current = current/scale;

  time += current - lastPause + (addTime*maxTime);
  printf("Time taken: %dms\n",time);
  printf("Current number of live cells: %d\n",MM.getPixelCount() + MM2.getPixelCount());


  //returns the finished image
  c_leds<: 2; //RGB LED to blue state while exporting
  printf("Exporting...\n");

  for(int y=0; y<IMHT/2;y++){
    for(int x=0;x < IMWD;x++){
      //pixel = chunks[c].pixels[y][x];
      c_out <: (uchar)MM.getBit(x,y);
    }
  }
  for(int y=IMHT/2; y<IMHT;y++){
    for(int x=0;x < IMWD;x++){
      //pixel = chunks[c].pixels[y][x];
      c_out <: (uchar)MM2.getBit(x,y-(IMHT/2));
    }
  }
  c_leds<: 0;
}

/////////////////////////////////////////////////////
//stores the whole image in compressed format and handles getting and setting pixels
/////////////////////////////////////////////////////
void memoryManager(server pixelMemory  mem[chunkNum/2 + 1]){
  //Chunk image[chunkNum];
  uchar compressedImage[IMHT/2 + 1][IMWD/8];
  uchar overlap[IMWD/8];
  printf("memory manager running\n");
  while(1){
    select{
      //Get a single chunk with overlapping top and bottom lines
      case mem[int j].getChunk(int c) -> Chunk chunk:
        int i = 0;
        int chunkAbove = ((c+1)%(chunkNum/2));
        int chunkBelow = ((c+((chunkNum/2)-1))%(chunkNum/2));
        if(c == 0){
          memcpy( chunk.pixels[0], &overlap,sizeof(uchar)*IMWD/8);
          memcpy( chunk.pixels[chunkHeight-1],&compressedImage[(chunkAbove*height)] ,  sizeof(uchar)*IMWD/8);
        }else if(c == (chunkNum/2)-1){
          memcpy( chunk.pixels[chunkHeight-1], &compressedImage[IMHT/2],sizeof(uchar)*IMWD/8);
          memcpy( chunk.pixels[0],&compressedImage[(chunkBelow*height)+height-1] ,  sizeof(uchar)*IMWD/8);
        }else{
          memcpy( chunk.pixels[0],&compressedImage[(chunkBelow*height)+height-1] ,  sizeof(uchar)*IMWD/8);
          memcpy( chunk.pixels[chunkHeight-1],&compressedImage[(chunkAbove*height)] ,  sizeof(uchar)*IMWD/8);
        }
        for(i=1;i <= height;i++){
          memcpy( chunk.pixels[i],&compressedImage[(c*height)+i-1] ,  sizeof(uchar)*IMWD/8);
        }

      break;
      //Store a single byte of the image
      case mem[int j].storeByte(int x, int y,uchar byte):
        //printf("Storing byte\n");
        if(y == -1){
            overlap[x/8] = byte;
        }else{
            compressedImage[y][x/8] = byte;
        }
      break;
      case mem[int j].storeLine(int y, uchar line[IMWD/8]):

        if(y == -1){
            memcpy(&overlap, &line, sizeof(uchar)*IMWD/8);
        }else{
            memcpy(&compressedImage[y], &line, sizeof(uchar)*IMWD/8);
        }
        break;
      //Get and store the top and bottom overlapping lines
      case mem[int j].storeOverlap(Overlap o):
        memcpy(&overlap, o.pixels[1],sizeof(uchar)*IMWD/8);
        memcpy(&compressedImage[IMHT/2], o.pixels[0],sizeof(uchar)*IMWD/8);
      break;
      case mem[int j].getOverlap() -> Overlap o:
        memcpy(o.pixels[0],&compressedImage[0],sizeof(uchar)*IMWD/8);
        memcpy(o.pixels[1],&compressedImage[IMHT/2-1],sizeof(uchar)*IMWD/8);
      break;
      //get a single pixel value
      case mem[int j].getBit(int x, int y) -> uchar pixel:
        int bit;
        int i = x%8;
        x = x/8;
        if(y ==  -1){
          bit = (overlap[x]>>i)&1;
        }else{
          bit =  (compressedImage[y][x]>>i)&1;
        }
        if(bit == 1){
          pixel = 255;
        }else{
          pixel = 0;
        }
        break;
      //count the number of pixels in the image
      case mem[int j].getPixelCount() -> int pixels:
        pixels = 0;
        for(int y=0;y<IMHT/2;y++){
          for(int x=0;x < IMWD/8;x++){
            //converts each byte into an integer representing it's hamming weight
            pixels += (compressedImage[y][x] * 01001001001ULL & 042104210021ULL) % 017;
          }
        }
      break;
}

}

}

/////////////////////////////////////////////////////////////////////////////////////////
//
// Write pixel stream from channel c_in to PGM image file
//Does this need it's own thread??
/////////////////////////////////////////////////////////////////////////////////////////
void DataOutStream( chanend c_in)
{
  int res;
  uchar line[ IMWD ];
  //Open PGM file
  printf( "DataOutStream:Start...\n" );
  res = _openoutpgm( outfname, IMWD, IMHT );
  if( res ) {
    printf( "DataOutStream:Error opening %s\n.", outfname );
    return;
  }
  //Compile each line of the image and write the image line-by-line
  for( int y = 0; y < IMHT; y++ ) {
    for( int x = 0; x < IMWD; x++ ) {
      c_in :> line[ x ];
    }
    _writeoutline( line, IMWD );
  }
  //Close the PGM image
  _closeoutpgm();
  printf( "DataOutStream:Done...\n" );
  return;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
// Initialise and  read accelerometer, send first tilt event to channel
//
/////////////////////////////////////////////////////////////////////////////////////////
void accelerometer(client interface i2c_master_if i2c, chanend toDist) {
  i2c_regop_res_t result;
  char status_data = 0;
  int tilted = 0;

  // Configure FXOS8700EQ
  result = i2c.write_reg(FXOS8700EQ_I2C_ADDR, FXOS8700EQ_XYZ_DATA_CFG_REG, 0x01);
  if (result != I2C_REGOP_SUCCESS) {
    printf("I2C write reg failed\n");
  }
  // Enable FXOS8700EQ
  result = i2c.write_reg(FXOS8700EQ_I2C_ADDR, FXOS8700EQ_CTRL_REG_1, 0x01);
  if (result != I2C_REGOP_SUCCESS) {
    printf("I2C write reg failed\n");
  }

  //Probe the accelerometer x-axis forever
  while (1) {

    //check until new accelerometer data is available
    do {
      status_data = i2c.read_reg(FXOS8700EQ_I2C_ADDR, FXOS8700EQ_DR_STATUS, result);
    } while (!status_data & 0x08);
    //get new x-axis tilt value
    int x = read_acceleration(i2c, FXOS8700EQ_OUT_X_MSB);

    //send signal to distributor after first tilt
    if (!tilted) {
      if (x>50) {
        tilted = 1;
        toDist <: 1;
      }
    }
    else if (tilted){
      if (x<10) {
        tilted = 0;
        toDist <: 0;
      }
    }
  }
  return;
}


/////////////////////////////////////////////////////////////////////////////////////////
//
// Set up button presses
//
/////////////////////////////////////////////////////////////////////////////////////////

void buttonListener(in port b, chanend toDist){
  int r;
  while (1) {
    b when pinseq(15)  :> r;    // check that no button is pressed
    b when pinsneq(15) :> r;    // check if some buttons are pressed
    if (r==14||r==13)     // if SW1 button is pressed
    toDist <: r;             // send button press
  }
  return;
}

/////////////////////////////////////////////////////////////////////////////////////////
//
// Orchestrate concurrent system and start up all threads
//
/////////////////////////////////////////////////////////////////////////////////////////
int main(void) {

  i2c_master_if i2c[1];               //interface to accelerometer
  interface pixelMemory MM[chunkNum/2 + 1];
  interface pixelMemory MM2[chunkNum/2 + 1];

  chan c_inIO, c_outIO, c_control, c_buttons,cWorker[chunkNum];    //extend your channel definitions here
  par {
    on tile[0] : distributor(c_inIO, c_outIO, c_control, c_buttons, cWorker, MM[chunkNum/2], MM2[chunkNum/2],leds);//thread to coordinate work on image
    on tile[0] : buttonListener(buttons, c_buttons); //thread to accept button inputs
    on tile[0] : i2c_master(i2c, 1, p_scl, p_sda, 10);   //server thread providing accelerometer data
    //Memory managers to    hold and control access to both halves of the image
    on tile[0] : memoryManager(MM);
    on tile[1] : memoryManager(MM2);
    on tile[1] : accelerometer(i2c[0],c_control);
    //Threads for reading in and writing out PGM Images
    //Could be regular sequential functions
    on tile[1] : DataInStream( c_inIO);
    on tile[1] : DataOutStream( c_outIO);
    //4 workers on each tile
    par(int i=0;i < chunkNum/2; i++){
      on tile[0] : workerThread(i, cWorker[i], MM[i]);
    }
    par(int i=(chunkNum/2);i < chunkNum; i++){
      on tile[1] : workerThread(i, cWorker[i], MM2[i-(chunkNum/2)]);
    }
    }
  return 0;
}

